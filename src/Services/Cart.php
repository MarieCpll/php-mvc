<?php

namespace App\Services;

class Cart
{
	/**
	 * Retourne le tableau produits ajouter par l'utilisateur
	 * @return array Data Object
	 */
	public static function get(){
		return [];
	}

	/**
	 * Ajoute un produits dans le paniers
	 */
	public static function add($id,$qt,$nm,$px){
		
		// print_r($id);
		// print_r($qt);
		// print_r($nm);
		// print_r($px);

		if (!is_array($_SESSION['cart'])) {
			$_SESSION['cart'] = [];
		}
		$_SESSION['cart'][$id] += $qt ;

		// $_SESSION[panier] = [$id =>$qt];
		// die();

	}

	/**
	 * Compte le nombre d'article qu'il y à dans le panier
	 */
	public function count(){
		// if (isset($_SESSION['cart'])) {
		// 	return count($_SESSION['cart']['quantite']);
		// }else{
		// 	return "panier vide";
		// }
		$nbproduct = 0;
		foreach ($_SESSION['cart'] as $quantity) {
			$nbproduct += $quantity;
		}
		return $nbproduct;
	}

	/**
	 * Calcul le prix total du panier
	 */
	public function total(){
		$total = 0 ;
		for ($i=0; $i < count($_SESSION['cart']['name']); $i++) { 
			$total += $_SESSION['cart']['quantite'][$i] * $_SESSION['cart']['price'][$i];
		}
		// foreach ($_SESSION['cart'] as $prixTotal) {
		// 	$total += $prixTotal * $px;
		// }
		return $total;
	}
}