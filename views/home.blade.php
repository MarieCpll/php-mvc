@extends('layouts')

@section('content')
	<section class="container">	
		<h1 class="title">Listes des produits</h1>
		<hr>
		<div class="columns is-desktop">
			{{-- Boucles pour récupérer des produits (https://laravel.com/docs/5.8/blade), 
					 Bulma : https://bulma.io/documentation/columns/responsiveness/,
									 https://bulma.io/documentation/components/card/
			 --}}


			@foreach ($products as $p)
    			<p class="column">{{ $p->name }} {{ $p->price}}</p>
    			<img src="{{ $p->picture }}">
    			<a href="/product/{{$p->id}}" class="button">Voir les infos</a>
			@endforeach

		</div>
	</section>
@endsection