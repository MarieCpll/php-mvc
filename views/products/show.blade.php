@extends('layouts')

@section('content')
	<section class="container">
		{{-- 
			Affiche toute les information d'un seul produit
			avec un bouton pour ajouter le produits et on peut choisir la quantité
		--}}
					
			<img src="{{ $product->picture }}">
			<p>{{ $product->name }} {{ $product->price }}</p>
		<form  action="/cart/add" method="post">
			<button class="button is-success" type="submit">Ajouter au panier</button>
			<div class="select is-success">
				<select name="quantite">
					<option>1</option> 
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
				</select>
			</div>
			<input type="hidden" name="id" value="{{$product->id}}">
			<input type="hidden" name="name" value="{{$product->name}}">
			<input type="hidden" name="prix" value="{{$product->price}}">
		</form>
		
	</section>
@endsection